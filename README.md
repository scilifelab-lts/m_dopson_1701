# Microbes in the deep 

## Overview
This repository is used to analyze individual microbial genomes
obtained via the M_Dopson_1701 project. These genomes can either be

- SAGs (**S**ingle **A**mplified **G**enomes) or
- MAGs (**M**etagenome **A**ssembled **G**enomes)

SAGs are generated for M_Dopson by JGI while MAGs are generated as part of the
metagenomic analyses (using the
[lts-workflows-sm-metagenomics](https://bitbucket.org/scilifelab-lts/lts-workflows-sm-metagenomics)
workflow).

Results from the metagenomes will be integrated with MAGs and SAGs
by annotating the isolated genomes and mapping metagenomic reads onto them.

## Setup
### Clone the repository

```
git clone https://bitbucket.org/scilifelab-lts/m_dopson_1701.git .
```

### Required software
Install the required conda environment for running snakemake.

```
conda env create -f envs/environment.yaml
```

**Note:** To install the conda environment under the current working
directory (instead of in your home path) you can do:

```
mkdir envs/dopson
conda env create -p envs/dopson -f envs/environment.yaml
```

Add the path to the conda config so that your bash prompt won't have the
full path of the environment.

```
conda config --add envs_dirs <full_path_to_git_repository>/m_dopson_1701/envs
```

Activate the installed environment.

```
conda activate dopson
```


The checkm software is not included in the _dopson_ environment. Instead the conda environment is integrated into the checkm-specific rules via the `conda:` [directive](http://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#integrated-package-management). 
This means that snakemake has to be run with the `--use-conda` flag in order to work properly. 

## Running the workflow

The workflow is designed to annotate genomes and map their abundance in
selected samples.

### Config file
Settings needed to run the workflow are found in [config/config.yaml](config/config.yaml)
and these can be changed depending on your needs.

### Running on Uppmax
To make use of the slurm queue system on Uppmax you can run the workflow
with the [config/cluster.yaml](config/cluster.yaml) file as:

```
snakemake -j 99 -k -p --cluster-config config/cluster.yaml --cluster "sbatch -A {cluster.account} -p {cluster.partition} -n {cluster.n}  -t {cluster.time} -e {cluster.log} -o {cluster.log} {cluster.extra}"
```


### Preprocessing steps
An initial analysis of genomes can be done using checkm. This gives
estimates of genome completeness and assigns a phylogeny. To run this
initial analysis run:

```
snakemake --use-conda -p preprocess
```

By default the workflow runs the analysis on genomes specified in
[genomes/MAGs.tab](genomes/MAGs.tab). However, you can specify another
list of genomes using the `--config genome_list=<your genome list>`
directive to snakemake, e.g.:

```
snakemake --use-conda --config genome_list=genomes/SAGs.tab -p preprocess
```

The genome list file needs to be in the following format:

| genome_id | name |
| ---- | ---- |
| 2754412945 | Rhodobacteraceae bacterium JGI_KA33_H11_081 |
| Modern_marine.001 | Modern_marine.001 |

The `genome_id` column contains a unique genome identifier while the `name` column
can specify a more descriptive name for the genome.

**Each genome must have a nucleotide fasta file with the suffix `.fna` in a
directory under data/genomes/<genome_id>.**

Output from checkm are placed under `results/checkm/<genome_list_name>/`.
This allows you to run preprocessing on several sets of genomes in parallel
and also means you can add genomes without having to rerun the checkm steps
for already analyzed genomes.

#### Collating preprocessing results
Preprocessing produces the checkm files **genome_stats.tab** and **phylogeny.tab**
describing the genome statistics (completeness, contamination, genome size etc.)
and the phylogenetic placement of each genome, respectively.

To collate all output from checkm you can run the script
[collate_checkm_results.py](source/utils/collate_checkm_results.py):

```
python source/utils/collate_checkm_results.py --genome_files genomes/MAGs.tab genomes/SAGs.tab genomes/SAGs_update.tab > genomes/preprocessed.tab
```

Here 3376 genomes (402 SAGs and 2974 MAGs) have been preprocessed and
their checkm results collated into `genomes/preprocessed.tab`. See
below on how to run the snakemake pipeline to cluster and annotate genomes
based on their genome statistics.

### Filtering genomes prior to analysis
Using the output from the preprocessing steps you can choose to run
the downstream analysis steps only on genomes meeting certain critera such as:
- minimum completeness
- maximum contamination
- minimum/maximum genome size

You can either specify the thresholds in the config file:

```yaml
min_completeness:
max_contamination:
min_genome_size:
max_genome_size:
```

or directly on the command line. To only annotate genomes with at least
80% completeness and at most 5% contamination you can run:

```
snakemake --config min_completeness=80 max_contamination=5 -p
```


### Functional annotation steps

Functional annotation is performed with:
- eggnog-mapper ([eggNOG](http://eggnogdb.embl.de/), [GO terms](http://www.geneontology.org/), [KEGG KO](http://www.genome.jp/kegg/ko.html), [metabolic reactions](http://bigg.ucsd.edu/))
- pfam_scan ([PFAM domains](https://pfam.xfam.org/))
- hmmsearch ([TIGRFAM domains](http://www.jcvi.org/cgi-bin/tigrfams/index.cgi))

The basic command to run the workflow is:

`snakemake -s Snakefile --configfile config/config.yaml --use-conda`

### Clustering genomes by nucleotide similarity
The workflow includes steps to cluster SAGs and MAGs by nucleotide
identity using [MUMmer](http://mummer.sourceforge.net/).

This allows you to group together genomes from different samples into
'genome clusters' (see
[Hugerth et al 2015](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-015-0834-7)).
By default genomes with a maximum nucleotide distance of 0.05 are clustered
but this can be changed via the `max_nuc_dist` config setting or via the command line:

```
snakemake --config max_nuc_dist=0.05 -p cluster_genomes
```

This produces a set of output files in the `results/mummer/output` directory:

* dist_matrix.tsv: A nucleotide distance matrix of all genomes analyzed
* fasta_names.tsv: A table of names for the input files (used for plotting)
* genome_clusters.tsv: A table with genome cluster names and genome ids. The
clusters are sorted by number of genomes in the clusters.
* hclust_dendrogram.pdf: A dendrogram plot of the genomes (euclidean
distance, average linkage clustering)
* hclust_heatmap.pdf: Dendrogram and heatmap of nucleotide distances for the genomes.

**Note**: If additional genomes are added to the workflow, either because
you change the filter settings (see above) or because you get new genomes
to analyze, you will need to force the execution of the `cluster_genomes`
target rule:

```
snakemake -p -f cluster_genomes
```


### Analysis of strains using DESMAN

To analyze strain diversity for MAGs, first run:

1. preprocess (using CheckM)
2. cluster_genomes (using Mummer)

DESMAN infers strains from MAGs by variant calling on single-copy core genes
(SCGs). Because CheckM can output sets of marker genes specific to certain lineages
(defined as being single-copy in >97% of genomes) we will use the information
obtained for MAGs during the **preprocess** step.

For each MAG cluster, the lowest common rank with a lineage-specific marker set
assigned during the checkm phylogeny step is used to extract SCGs to use for DESMAN.

The following files should be produced for the DESMAN part of the workflow:

- a genome-cluster file in the format:

| cluster_id | genome_ids | lineage |
| ---------- | ---------- | ------- |
| CLUSTER_1  | genome_1,genome_2 | order: Burkholderiales |
| ... | ... | ... |

This example shows a genome cluster named 'CLUSTER_1' with 2 genomes that
share the taxonomic assignment of Burkholderiales at rank order. There are
427 SCGs in the lineage-specific marker set for Burkholderiales (inferred from
194 genomes).

Parse
1. cluster file
2. checkm output directory
