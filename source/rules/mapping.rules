localrules:
    bowtie_build,
    make_contig_map,
    avg_seq_length,
    normalize_samples,
    generate_contig_SAF,
    merge_sample_maps

#####################
## Mapping bowtie2 ##
#####################

rule bowtie_build_combined:
    input:
        expand("results/annotation/{genome}/{genome}.fna", genome = genomes.keys())
    output:
        expand(os.path.join("results","map",os.path.basename(config["genome_list"]),"genomes.{index}.bt2l"), index=range(1,5)),
    params:
        tmpdir=os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"])),
        outdir=os.path.join("results","map",os.path.basename(config["genome_list"]))
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60
    threads: 4
    shell:
        """
        mkdir -p {params.tmpdir}
        cat {input} > {params.tmpdir}/genomes.fasta
        bowtie2-build --threads {threads} --large-index {params.tmpdir}/genomes.fasta {params.tmpdir}/genomes
        rm {params.tmpdir}/genomes.fasta
        mv {params.tmpdir}/genomes* {params.outdir}/
        """

rule make_contig_map:
    input:
        expand("results/annotation/{genome}/{genome}.fna", genome = genomes.keys())
    output:
        os.path.join("results","map",os.path.basename(config["genome_list"]),"genomes.map")
    run:
        with open(output[0], 'w') as fh:
            for line in shell("grep '>' {input} | cut -f1 -d ' '", iterable = True):
                genome, contig = line.split(">")
                genome = os.path.basename(genome).rstrip(".fna:")
                fh.write("{}\t{}\n".format(contig, genome))


rule bowtie2_map_sample:
    """
    Perform read mapping, sorting and duplicate removal in one go
    """
    input:
        R1 = lambda wildcards: samples[wildcards.sample]["R1"],
        R2 = lambda wildcards: samples[wildcards.sample]["R2"],
        bt_index = expand(os.path.join("results","map",os.path.basename(config["genome_list"]),"genomes.{index}.bt2l"),
            index=range(1,5))
    output:
        bam = os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.mapped.sorted.rmdup.bam"),
        logfile = os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.log"),
        metric_file = os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.metrics")
    params:
        prefix = os.path.join("results","map",os.path.basename(config["genome_list"]),"genomes"),
        tempdir = os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"])),
        temp_sam = os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"]),"{sample}.sam"),
        temp_bam = os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"]),"{sample}.bam"),
        temp_rmdup_bam = os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"]),"{sample}.rmdup.bam"),
        temp_resort_bam = os.path.join(os.path.expandvars("$TMPDIR"),os.path.basename(config["genome_list"]),"{sample}.resorted.bam"),
        jarfile=config["picard_jar"],
        picard_path=config["picard_path"],
        java_opt="-Xms2g -Xmx64g"
    threads: 10
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60*10
    shell:
        """
        mkdir -p {params.tempdir}
        # Map and produce sam file
        bowtie2 --very-sensitive --no-unal -p {threads} -x {params.prefix} \
        -1 {input.R1} -2 {input.R2} > {params.temp_sam} 2>{output.logfile}
        # Sort and convert sam file
        samtools view -h -b {params.temp_sam} | samtools sort -@ 9 -o {params.temp_bam}
        # Remove duplicates from sorted bam-file
        java {params.java_opt} -XX:ParallelGCThreads={threads} -cp params.picard_path -jar {params.jarfile} \
        MarkDuplicates I={params.temp_bam} O={params.temp_rmdup_bam} M={output.metric_file} ASO=coordinate \
        REMOVE_DUPLICATES=TRUE 2> /dev/null
        # Re-sort the bam file
        samtools sort -@ 9 -o {params.temp_resort_bam} {params.temp_rmdup_bam}
        # Move to final location
        mv {params.temp_resort_bam} {output.bam}
        """

#######################################
## Quantification with FeatureCounts ##
#######################################
rule generate_contig_SAF:
    input:
        "results/annotation/{genome}/{genome}.fna"
    output:
        "results/annotation/{genome}/{genome}.SAF"
    message: "Generate simplified annotation format (SAF) file for {wildcards.genome}"
    shell:
        "python source/utils/fasta2bed.py --saf -i {input[0]} -o {output[0]}"

rule combined_genome_featurecount:
    input:
        saf = expand("results/annotation/{genome}/{genome}.SAF", genome = genomes.keys()),
        bam = os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.mapped.sorted.rmdup.bam")
    output:
        os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.fc.tab"),
        os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.fc.tab.summary")
    threads: 4
    resources:
        runtime = lambda wildcards, attempt: attempt**2*60
    shell:
        """
        cat {input.saf} > $TMPDIR/saf
        featureCounts -T {threads} -a $TMPDIR/saf -F SAF -M -p -B -o {output[0]} {input.bam}
        """

###########################
## Normalize read counts ##
###########################
rule avg_seq_length:
    input:
        R1 = lambda wildcards: samples[wildcards.sample]["R1"],
        R2 = lambda wildcards: samples[wildcards.sample]["R2"]
    output:
        "data/samples/{sample}.read_length.txt"
    run:
        import numpy as np
        lengths = []
        for line in shell("seqtk seq -f 0.01 {input.R1} | seqtk comp | cut -f2 | sort | uniq -c", iterable = True):
            lengths += get_len(line)
        for line in shell("seqtk seq -f 0.01 {input.R2} | seqtk comp | cut -f2 | sort | uniq -c", iterable = True):
            lengths += get_len(line)
        sample_length = np.round(np.mean(lengths),2)
        with open(output[0], 'w') as fh:
            fh.write("\tavg_len\n")
            fh.write("{}\t{}\n".format(wildcards.sample,sample_length))


def calculate_sample_tpm(df, sample, readlength):
    # 1. Calculate t for sample
    # t = (reads_mapped_to_gene * read_length) / length_of_gene
    # Multiply gene counts with read length, then divide by the Length column (in kb)
    t = df[sample].multiply(readlength).div(df["Length"].div(1000))
    df = df.assign(t=pd.Series(t, index=df.index))
    # 2. Calculate T
    # T = sum(t)
    T = df["t"].sum()
    # 3. Calculate TPM
    # TPM = t*10^6 / T
    TPM = (df["t"].multiply(1000000)).div(T)
    df = df.assign(TPM=pd.Series(TPM, index=df.index))
    return df

rule normalize_samples:
    input:
        fc = os.path.join("results","map",os.path.basename(config["genome_list"]),"{sample}.fc.tab"),
        contigmap = os.path.join("results","map",os.path.basename(config["genome_list"]),"genomes.map"),
        lengthfile = "data/samples/{sample}.read_length.txt"
    output:
        raw = os.path.join("results","abundance",os.path.basename(config["genome_list"]),"{sample}.raw_counts.tab"),
        raw_percontig = os.path.join("results","abundance",os.path.basename(config["genome_list"]),"{sample}.raw_counts.percontig.tab"),
        tpm = os.path.join("results","abundance",os.path.basename(config["genome_list"]),"{sample}.tpm.tab"),
        tpm_percontig = os.path.join("results","abundance",os.path.basename(config["genome_list"]),"{sample}.tpm.percontig.tab")
    run:
        readlength = pd.read_table(input.lengthfile)["avg_len"].values[0]
        contigmap = pd.read_csv(input.contigmap, header=None, sep="\t", index_col=0, names=["contig","genome"])
        df = pd.read_table(input.fc, comment="#")
        sample = os.path.basename(input.fc).rstrip(".fc.tab")
        df.columns = list(df.columns[0:-1])+[sample]
        # Add genome ids
        df = pd.merge(df, contigmap, left_on="Geneid", right_index=True)
        # Subset dataframe
        raw_percontig = df.loc[:,["Chr","Length",sample]]
        # Write raw counts per contig
        raw_percontig.to_csv(output.raw_percontig, sep="\t", index=False)
        # Sum raw counts per genome
        raw = df.groupby("genome").sum().reset_index().loc[:,["genome",sample]]
        raw.to_csv(output.raw, sep="\t", index=False)
        # Calculate TPM
        TPM = calculate_sample_tpm(df, sample, readlength)
        tpm_percontig = TPM.loc[:,["Chr","Length","TPM"]]
        tpm_percontig.rename(columns = {"TPM": sample}, inplace=True)
        # Write TPM per contig
        tpm_percontig.to_csv(output.tpm_percontig, sep="\t", index=False)
        # Sum TPM per genome
        TPM = TPM.groupby("genome").sum().reset_index().loc[:,["genome","TPM"]]
        TPM.columns = ["genome",sample]
        # Write TPM per genome
        TPM.to_csv(output.tpm, sep="\t", index=False)

def merge(files, ignore=None):
    df = pd.DataFrame()
    if ignore:
        ignore_cols = ignore
        if type(ignore_cols)==str:
            ignore_cols = [ignore_cols]
    for i, f in enumerate(files):
        _df = pd.read_table(f, index_col=0)
        if i>0 and ignore:
            _df.drop(ignore_cols, axis=1, inplace=True)
        df = pd.merge(df, _df, left_index=True, right_index=True, how="outer")
    df.fillna(0, inplace=True)
    if ignore:
        df = df.loc[:,ignore_cols+sorted(set(df.columns).difference(set(ignore_cols)))]
    else:
        df = df.loc[:,sorted(df.columns)]
    return df

rule merge_sample_maps:
    input:
        raw = expand("results/abundance/{genome_list}/{sample}.raw_counts.tab", 
            genome_list = os.path.basename(config["genome_list"]), sample = samples.keys()),
        raw_percontig = expand("results/abundance/{genome_list}/{sample}.raw_counts.percontig.tab",
            genome_list = os.path.basename(config["genome_list"]), sample = samples.keys()),
        tpm = expand("results/abundance/{genome_list}/{sample}.tpm.tab", 
            genome_list = os.path.basename(config["genome_list"]), sample = samples.keys()),
        tpm_percontig = expand("results/abundance/{genome_list}/{sample}.tpm.percontig.tab", 
            genome_list = os.path.basename(config["genome_list"]), sample = samples.keys())
    output:
        raw = "results/abundance/{genome_list}/raw_counts.tab".format(genome_list = os.path.basename(config["genome_list"])),
        raw_percontig = "results/abundance/{genome_list}/raw_counts.percontig.tab".format(genome_list = os.path.basename(config["genome_list"])),
        tpm = "results/abundance/{genome_list}/tpm.tab".format(genome_list = os.path.basename(config["genome_list"])),
        tpm_percontig = "results/abundance/{genome_list}/tpm.percontig.tab".format(genome_list = os.path.basename(config["genome_list"]))
    run:
        raw = merge(input.raw)
        raw_percontig = merge(input.raw_percontig, "Length")
        tpm = merge(input.tpm)
        tpm_percontig = merge(input.tpm_percontig, "Length")
        raw.to_csv(output.raw, sep="\t")
        raw_percontig.to_csv(output.raw_percontig, sep="\t")
        tpm.to_csv(output.tpm, sep="\t")
        tpm_percontig.to_csv(output.tpm_percontig, sep="\t")
