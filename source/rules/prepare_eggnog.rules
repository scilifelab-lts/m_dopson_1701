localrules:
    download_eggnog,
    get_kegg_module_info,
    get_kegg_ortholog_info

rule download_eggnog:
    output:
        "resources/eggnog-mapper/eggnog.db"
    log: "resources/eggnog-mapper/download_eggnog_data.log"
    conda: "../../envs/eggnog-mapper.yaml"
    shell:
        """
         download_eggnog_data.py --data_dir resources/eggnog-mapper/ -y bact arch viruses > {log} 2>&1
        """

def get_kegg_module_hierarchy(s):
    hier = {}
    # First level is 'ko00002'
    for d1 in s['children']:
        c1 = d1['name']
        for d2 in d1['children']:
            c2 = d2['name']
            for d3 in d2['children']:
                c3 = d3['name']
                for module in d3['children']:
                    module_name = module['name']
                    kos = module['children']
                    hier[module_name] = {"Module_category1": c1, "Module_category2": c2, "Module_category3": c3, "KOs": []}
                    for ko in kos:
                        ko_name = ko['name']
                        ko = ko_name.split(" ")[0]
                        hier[module_name]["KOs"].append(ko)
    return hier

rule get_kegg_module_info:
    output:
        temp("resources/ko00002.json"),
        "resources/kegg/kegg_ko2modules.tsv",
        "resources/kegg/kegg_modules.tsv"
    run:
        import pandas as pd, json
        # Process KEGG Module information
        shell("curl -L 'https://www.kegg.jp/kegg-bin/download_htext?htext=ko00002.keg&format=json' > {output[0]}")
        with open(output[0]) as fh:
            s = json.load(fh)
        hier = get_kegg_module_hierarchy(s)
        with open(output[1], 'w') as fh_ko2mod, open(output[2], 'w') as fh_modules:
            fh_modules.write("Module_id\tModule_name\tModule_category1\tModule_category2\tModule_category3\n")
            for module_name, d in hier.items():
                module_key = module_name.split(" ")[0]
                module_name = " ".join(module_name.split(" ")[1:]).lstrip()
                fh_modules.write("{}\t{}\t{}\t{}\t{}\n".format(module_key, module_name, d["Module_category1"], d["Module_category2"], d["Module_category3"]))
                for ko in set(d["KOs"]):
                    fh_ko2mod.write("{}\t{}\n".format(ko,module_key))

def get_kegg_ortholog_hierarchy(s):
    hier = {}
    ko2ec = {}
    # First level is 'ko00001'
    for d1 in s['children']:
        c1 = d1['name']
        for d2 in d1['children']:
            c2 = d2['name']
            for d3 in d2['children']:
                c3 = d3['name']
                if not "children" in d3.keys():
                    continue
                for ko in d3['children']:
                    ko_name = ko['name'].split("\t")[0]
                    ko_id = ko_name.split(" ")[0]
                    if "[EC:" in ko_name:
                        enzymes = ko_name.split("[")[-1].split("]")[0].lstrip("EC:").split(" ")
                    else:
                        enzymes = []
                    d = {"KO_category1": c1, "KO_category2": c2, "pathway": c3, "name": ko_name, "enzymes": enzymes}
                    try:
                        hier[ko_id].append(d)
                    except KeyError:
                        hier[ko_id] = [d]
    return hier

rule get_kegg_ortholog_info:
    output:
        json = temp("resources/ko00001.json"),
        ko2ec = "resources/kegg/kegg_ko2ec.tsv",
        ec2path = "resources/kegg/kegg_ec2pathways.tsv",
        ko2path = "resources/kegg/kegg_ko2pathways.tsv",
        pathways = "resources/kegg/kegg_pathways.tsv",
        kos = "resources/kegg/kegg_kos.tsv"
    run:
        import pandas as pd, json
        shell("curl -L 'https://www.genome.jp/kegg-bin/download_htext?htext=ko00001.keg&format=json' > {output.json}")
        with open(output.json) as fh:
            s = json.load(fh)
        hier = get_kegg_ortholog_hierarchy(s)
        pathways = {}
        ko2ec = {}
        ec2path = {}
        # Write KEGG Ortholog names, KEGG Ortholog -> Pathway map, and KEGG Ortholog -> Enzyme map
        with open(output.kos, 'w') as fh_kos, open(output.ko2path, 'w') as fh_ko2path, open(output.ko2ec, 'w') as fh_ko2ec:
            fh_kos.write("ko\tKO_name\n")
            for ko_id, l in hier.items():
                for i, d in enumerate(l):
                    if i==0:
                        fh_kos.write("{}\t{}\n".format(ko_id,d["name"]))
                        for enzyme in d["enzymes"]:
                            fh_ko2ec.write("{}\t{}\n".format(ko_id,enzyme))
                    fh_ko2path.write("{}\t{}\n".format(ko_id,"map{}".format(d["pathway"].split(" ")[0])))
                    pathways[d["pathway"]] = {"Pathway_category1": d["KO_category1"], "Pathway_category2": d["KO_category2"]}
                    for enzyme in d["enzymes"]:
                        try:
                            ec2path[enzyme].append("map{}".format(d["pathway"].split(" ")[0]))
                        except KeyError:
                            ec2path[enzyme] = ["map{}".format(d["pathway"].split(" ")[0])]
        # Write Pathway information
        with open(output.pathways, 'w') as fh_pathways:
            fh_pathways.write("{}\t{}\t{}\t{}\n".format("Pathway_id", "Pathway_name", "Pathway_category1", "Pathway_category2"))
            for pathway, d in pathways.items():
                pathway_id = pathway.split(" ")[0]
                pathway_name = pathway.replace("{} ".format(pathway_id),"")
                fh_pathways.write("map{}\t{}\t{}\t{}\n".format(pathway_id, pathway_name, d["Pathway_category1"], d["Pathway_category2"]))
        # Write Enzyme -> Pathway map
        with open(output.ec2path, 'w') as fh_ec2path:
            for enzyme, l in ec2path.items():
                for pathway in set(l):
                    fh_ec2path.write("{}\t{}\n".format(enzyme,pathway))
