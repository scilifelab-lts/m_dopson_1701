#!/usr/bin/env python

import ast, sys
from argparse import ArgumentParser


def read_results(input):
    results = {}
    with open(input, 'r') as fh:
        for line in fh:
            line = line.rstrip()
            key, dict_string = line.split("\t")
            results[key] = ast.literal_eval(dict_string)
    return results


def write_results(results, output, columns):
    existing_cols = list(set(list(results.values())[0].keys()).intersection(columns))

    if len(existing_cols) == 0:
        sys.exit("ERROR: None of columns {} found in results\n".format(",".join(columns)))
    if output:
        fh_out = open(output, 'w')
    else:
        fh_out = sys.stdout
    sorted_cols = [x for x in columns if x in existing_cols]
    fh_out.write("Genome\t"+"\t".join(sorted_cols)+"\n")
    for key in sorted(results.keys()):
        d = results[key]
        output = [key]
        for col in sorted_cols:
            output.append(str(d[col]))
        fh_out.write("\t".join(output)+"\n")
    fh_out.close()


def main():
    parser = ArgumentParser()

    parser.add_argument("-i", "--input", required = True,
                        help="Input file from checkm (bin_stats.analyze.tsv or bin_stats_ext.tsv)")
    parser.add_argument("-o", "--output",
                        help="Output table (defaults to stdout)")
    parser.add_argument("-c", "--columns", default=["Genome size","GC"], nargs = "+",
                        help="Columns to include in output (defaults to 'Genome size' and 'GC'")
    parser.add_argument("--print_cols", action="store_true",
                        help="Print possible columns from file")
    args = parser.parse_args()

    results = read_results(args.input)
    if args.print_cols:
        for col in sorted(list(set(list(results.values())[0].keys()))):
            sys.stdout.write("{}\n".format(col))
        sys.exit()
    write_results(results, args.output, args.columns)

if __name__ == '__main__':
    main()