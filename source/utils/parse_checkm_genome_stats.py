#!/usr/bin/env python

import pandas as pd
import sys
from argparse import ArgumentParser


def get_good_quality_genomes(qa_table, min_comp, max_cont, min_size, max_size):
    df = pd.read_table(qa_table, index_col=0)
    # Filter by completeness
    comp_filter = list(df.loc[df["Completeness"]>=min_comp].index)
    # Filter by contamination
    cont_filter = list(df.loc[df["Contamination"]<=max_cont].index)
    # Filter by size, if specified
    maxsize_filter = list(df.index)
    minsize_filter = list(df.index)

    try:
        maxsize_filter = list(df.loc[df["Genome size (bp)"]<=int(max_size)].index)
    except ValueError:
        pass
    except TypeError:
        pass
    try:
        minsize_filter = list(df.loc[df["Genome size (bp)"]>=int(min_size)].index)
    except ValueError:
        pass
    except TypeError:
        pass

    # Get all filtered bins
    filtered_genomes = comp_filter + cont_filter + maxsize_filter + minsize_filter
    # Good bins are present in all filters
    good_quality = []
    for b in df.index:
        if filtered_genomes.count(b) == 4:
            good_quality.append(b)
    return sorted(good_quality)


def get_genome_stats_files(pat):
    import glob
    files = glob.glob(pat)
    return files


def make_genome_list(genome_files):
    df = pd.DataFrame()
    for f in genome_files:
        _df = pd.read_table(f, index_col=0, dtype=str)
        df = pd.concat([df,_df])
    return df


def main():
    parser = ArgumentParser()
    parser.add_argument("--pat", default="results/checkm/*/*/genome_stats.tab",
                        help="File path pattern. Defaults to 'results/checkm/*/*/genome_stats.tab'")
    parser.add_argument("--genome_files", nargs="+", required=True,
                        help="Genome files to extract information from for good quality genomes")
    parser.add_argument("--min_completeness", default=80.0, type=float,
                        help="Minimum genome completeness (defaults to 80)")
    parser.add_argument("--max_contamination", default=5.0, type=float,
                        help="Maximum contamination (defaults to 5)")
    parser.add_argument("--min_size", type=int,
                        help="Minimum genome size")
    parser.add_argument("--max_size", type=int,
                        help="Maximum genome size")

    args = parser.parse_args()

    # Get genome stat files from glob
    stat_files = get_genome_stats_files(args.pat)
    genomes = []
    for f in stat_files:
        genomes += get_good_quality_genomes(qa_table=f, min_comp=args.min_completeness, max_cont=args.max_contamination,
                                            min_size=args.min_size, max_size=args.max_size)
    # Make genome list unique
    genomes = list(set(genomes))

    # Make one genome list
    genome_list = make_genome_list(args.genome_files)
    # Filter to good quality genomes
    genome_list = genome_list.loc[genomes]
    # Sort by index
    genome_list.rename(index=lambda x: str(x), inplace=True)
    genome_list.sort_index(inplace=True)
    # Write to file
    genome_list.to_csv(sys.stdout, sep="\t")


if __name__ == '__main__':
    main()
