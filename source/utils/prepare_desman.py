#!/usr/bin/env python

from argparse import ArgumentParser
import pandas as pd
import sys
import subprocess
import ast
import os
import logging
import operator

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
)


def make_compliant_genome_id(gid):
    """Cuts the genome id down to a size so that no contig name will be longer than 37 characters"""
    genome_id_length = len(gid)
    # Since the maximum length of contig names for prokka is 37 characters and contig
    # ids from megahit are typically at most 12 characters long (assuming < 10 million contigs for an assembly)
    # the genome id has to be at most 24 characters.
    if genome_id_length > 24:
        # Assume that the id suffix is required to identify the genome
        suffix = ".".join(gid.split(".")[1:])
        # Cut the prefix to length 23-suffix length
        prefix = gid[0:(23-len(suffix))]
        genome_id = "{}.{}".format(prefix,suffix)
        return genome_id
    else:
        return gid


def get_taxonomy_dict(taxonomy):
    """Generates a dictionary of taxonomic assignments for a genome from the checkm taxonomy string"""
    taxdict = {"kingdom": "", "phylum": "", "class": "", "order": "", "family": "", "genus": "", "species": ""}
    ranks = ["kingdom","phylum","class","order","family","genus","species"]
    rank_letters = {"k": "kingdom", "p": "phylum", "c": "class", "o": "order", "f": "family", "g": "genus", "s": "species"}
    try:
        taxonomy.split(";")
    except AttributeError:
        for rank in ranks:
            taxdict[rank] = "Unclassified"
        return taxdict
    for j, a in enumerate(taxonomy.split(";")):
        rank = a.split("_")[0]
        name = a.replace("{}__".format(rank),"").replace(" (root)","")
        taxdict[rank_letters[rank]] = name
    for rank in ranks[j+1:]:
        taxdict[rank] = "Unclassified.{}".format(name)
    for rank in ranks[j+1:]:
        taxdict[rank] = "Unclassified.{}".format(name)
    return taxdict


def get_phylogeny(f, genomes):
    """Parses the phylogeny table and creates taxonomy dictionary"""
    phylo_dict = {}
    df = pd.read_table(f, index_col=0)
    if genomes:
        df = df.loc[genomes]
    df = df.groupby(level=0).first()
    # Generate dictionary
    for t in df.index:
        if "Taxonomy" in df.columns:
            taxcol = "Taxonomy"
            tax = df.loc[t, taxcol]
            taxonomy = get_taxonomy_dict(tax)
        elif "Taxonomy (contained)" in df.columns:
            taxcol = "Taxonomy (contained)"
            tax = df.loc[t, taxcol]
            taxonomy = get_taxonomy_dict(tax)
        phylo_dict[t] = taxonomy
    return phylo_dict


def find_file(dir, file):
    """Locates needed files in the checkm output directory"""
    if os.path.isfile("{dir}/{file}".format(dir=dir, file=file)):
        return "{dir}/{file}".format(dir=dir, file=file)
    else:
        base = os.path.basename(file)
        p = subprocess.run("find {dir} -name {base}".format(dir=dir, base=base), stdout=subprocess.PIPE, shell=True)
        f = p.stdout.decode('utf-8').rstrip()
        if os.path.isfile(f):
            return f
        else:
            sys.exit("Cannot find {base} under {dir}\n".format(base=base, dir=dir))


def read_cluster_file(f):
    """Reads the genome cluster table"""
    clusters = pd.read_table(f, index_col=0, header=None,
                  names=["cluster", "genome"])
    return clusters


def parse_marker_file(f, genomes):
    """Parses the marker stats file from checkm into a dictionary with genome ids as keys"""
    genome_marker_dict = {}
    with open(f, 'r') as fh:
        for line in fh:
            genome, dict_str = line.rstrip().split("\t")
            if genomes:
                if not genome in genomes:
                    continue
            genome_marker_dict[genome] = ast.literal_eval(dict_str)
    return genome_marker_dict


def parse_checkm_taxon_list(f):
    """Reads available marker lists for different taxa"""
    marker_sets = {}
    with open(f, 'r') as fh:
        for i, line in enumerate(fh):
            if i < 3:
                continue
            if not line or line[0] == "-":
                continue
            line = line.rstrip().lstrip()
            rank = line.rsplit()[0]
            if rank == "domain":
                rank = "kingdom"
            name = " ".join(line.rsplit()[1:-3])
            try:
                marker_sets[rank].append(name)
            except KeyError:
                marker_sets[rank] = [name]
    return marker_sets


def get_genome_lineage_sets(phylo_dict, marker_sets):
    """Figure out the lowest common rank with a checkm lineage marker set for all MAGs belonging to a cluster"""
    genome_lineage_sets = {}
    for genome, genome_ranks in phylo_dict.items():
        lca_rank = ""
        lca_name = ""
        for rank in ["species", "genus", "family", "order", "class", "phylum", "kingdom"]:
            if not rank in genome_ranks.keys() or not rank in marker_sets.keys():
                continue
            if genome_ranks[rank] in marker_sets[rank]:
                lca_rank = rank
                lca_name = genome_ranks[rank]
                break
        if not lca_rank:
            lca_rank = "life"
            lca_name = "Prokaryote"
        genome_lineage_sets[genome] = {"rank": lca_rank, "name": lca_name}
    return genome_lineage_sets


def calculate_overlap(ali1, ali2):
    """Calculates overlapping positions between two alignments"""
    if len(ali1) < len(ali2):
        short = ali1
        long = ali2
    else:
        short = ali2
        long = ali1
    # 'overlap' is the number of positions that overlap
    overlap = set(short).intersection(set(long))
    # non_overlap is the positions of the shorter alignment that does not intersect the longer
    non_overlap = set(short).difference(long)
    # divide overlap by the shortest alignment to get overlap fraction
    overlap_frac = float(len(overlap)) / len(short)
    return sorted(list(overlap)), sorted(list(non_overlap)), overlap_frac


def get_orf_marker_lengths(markers):
    """Generates a dictionary with lengths of marker assignments"""
    lengths = {}
    for marker, coords in markers.items():
        for coord in coords:
            lengths["{}:{}:{}".format(marker, coord[0], coord[1])] = coord[1] - coord[0]
    return lengths


def parse_marker_assignments(markers, orf, genome):
    """Takes overlap of marker assignments into account"""
    # markers is a dictionary with markers as keys and values being coordinates
    # e.g. {'PF03668.10': [[9,287]]}
    # Sort the marker assignment by length
    lengths = get_orf_marker_lengths(markers)
    # sorted_markers is a list of tuples in the format
    # [('TIGR0001:1:100', 100), ('PF03668.10:1:50', 50)]
    sorted_markers = sorted(lengths.items(), key=operator.itemgetter(1), reverse=True)
    orf_markers = {}
    if len(sorted_markers) > 1:
        logging.debug("{}: {} markers for orf {} ({})".format(genome, len(sorted_markers), orf,
                                                              ",".join([x[0] for x in sorted_markers])))
    # Add marker locations starting from the longest
    for i in range(0, len(sorted_markers)):
        item = sorted_markers[i]
        marker, start, stop = item[0].split(":")
        new_ali = range(int(start), int(stop) + 1)
        if i == 0:
            orf_markers[item[0]] = (start, stop)
            continue
        # Compare every previous marker location
        add_marker = True
        for j in range(0, i):
            stored_item = sorted_markers[j][0]
            stored_marker, stored_start, stored_stop = stored_item.split(":")
            if stored_item in orf_markers:
                existing_ali = range(int(stored_start), int(stored_stop) + 1)
                overlap, non_overlap, overlap_frac = calculate_overlap(existing_ali, new_ali)
                # If the new marker is overlapped by less than 50% of its length, adjust its coordinates
                if overlap_frac <= 0.5:
                    start = non_overlap[0]
                    stop = non_overlap[-1]
                    new_ali = range(int(start), int(stop) + 1)
                    if len(new_ali) == 0:
                        add_marker = False
                        break
                # If it's overlapped by more than 50% of its length don't add it
                else:
                    add_marker = False
                    break
        if add_marker:
            orf_markers[item[0]] = (start, stop)
    return sorted_markers, orf_markers


def write_markers(genome_marker_dict, checkm_dir, out_dir, single_marker=False):
    """Writes marker locations for every genome in every cluster"""
    total_genomes = len(genome_marker_dict.keys())
    n = 0
    for genome in genome_marker_dict.keys():
        new_gid = make_compliant_genome_id(genome)
        os.makedirs("{out_dir}/{genome}".format(out_dir=out_dir, genome=genome), exist_ok=True)
        with open("{out_dir}/{genome}/markers.csv".format(out_dir=out_dir, genome=genome), 'w') as fh_csv, open("{out_dir}/{genome}/markers.bed".format(out_dir=out_dir, genome=genome), 'w') as fh_bed:
            n+=1
            logging.info("Parsing genome {} ({}/{})".format(genome, n, total_genomes))
            # Locate the checkm gff file and parse it
            gff = find_file(checkm_dir, "bins/{genome}/genes.gff".format(genome=genome))
            gff_table = pd.read_table(gff, header=None, comment="#", index_col=0,
                                      names=["contig", "source", "feature", "start", "stop", "score",
                                             "strand", "frame", "id"])
            gff_table.rename(index=lambda x: x.replace("|", "_"), inplace=True)
            gff_table.rename(index=lambda x: x.replace(genome, new_gid), inplace=True)
            # Add suffix column to make it easier to look-up orf
            gff_table = gff_table.assign(suffix=pd.Series([x.split(";")[0].split("_")[-1] for x in gff_table["id"]],
                                    index=gff_table.index))
            for orf, markers in genome_marker_dict[genome].items():
                # If the orf contains '&&' it's a merge of two ORFs and not actually a single-copy marker
                # For now we skip these ORFs
                if "&&" in orf:
                    continue
                # orf example: "Modern_marine_glass_biofilm.022.2k|k141_805478_4"
                contig = "_".join(orf.split("_")[0:-1])
                suffix = orf.split("_")[-1]
                # Rename ORF to make compliant with prokka
                new_orf = orf.replace(genome, new_gid).replace("|", "_")
                new_contig = contig.replace(genome, new_gid).replace("|", "_")
                # Locate the ORF coordinates on the contig
                orf_coord_on_contig = gff_table.loc[(gff_table.index==new_contig)&(gff_table.suffix==suffix),["start","stop"]].values[0]
                orf_strand = gff_table.loc[(gff_table.index==new_contig)&(gff_table.suffix==suffix),"strand"].values[0]
                if orf_strand == "+":
                    strand = "1"
                else:
                    strand = "-1"
                sorted_markers, orf_markers = parse_marker_assignments(markers, orf, genome)

                updated_markers = []


                # Ensure that the coordinates are printed in decreasing length order for each ORF
                for m, item in enumerate(sorted_markers):
                    marker = item[0]
                    # Adjust each marker coordinates to the orf_coord_on_contig
                    start, stop = orf_markers[marker]
                    updated_markers.append("{}:{}:{}".format(marker,start,stop))
                    # contig_start and contig_stop is the start and stop of the marker on the contig
                    # recalculate from amino acid positions to nucleotide
                    contig_start = orf_coord_on_contig[0]+(int(start)-1)*3
                    contig_stop = orf_coord_on_contig[0]+(int(stop)-1)*3
                    fh_csv.write("{new_orf},{marker},{contig_start},{contig_stop},-,-,{strand}\n".format(
                        marker=marker.split(":")[0], contig_start=contig_start, contig_stop=contig_stop,
                        new_orf=new_orf, strand=strand))
                    fh_bed.write("{new_contig}\t{contig_start}\t{contig_stop}\n".format(new_contig=new_contig, contig_start=contig_start, contig_stop=contig_stop))
                    if m == 0 and single_marker:
                        break
                if len(sorted_markers) > len(orf_markers):
                    logging.debug("{}: {} markers remaining after adjusting for overlap ({})".format(genome, len(orf_markers), ",".join(updated_markers)))
        # Write the gff file
        gff_table.iloc[:, 0:-1].to_csv("{out_dir}/{genome}/{genome}.gff".format(out_dir=out_dir, genome=genome), sep="\t",
                                       header=False, index=True)

def write_lineage_sets(genome_lineage_sets, out_dir):
    """Writes a file of cluster information"""
    with open("{out_dir}/genome_file.tab".format(out_dir=out_dir), 'w') as fh:
        for genome, dict in genome_lineage_sets.items():
            fh.write("{genome}\t{rank}:{name}\n".format(genome=genome, rank=dict["rank"], name=dict["name"]))


def main(args):
    # Read markers for each bin and write to file for the corresponding cluster
    marker_file = find_file(args.checkm_dir, "storage/marker_gene_stats.tsv")
    logging.info("Parsing markers for genomes from {}".format(marker_file))
    genome_marker_dict = parse_marker_file(marker_file, args.genomes)
    logging.info("Stored marker information for {} genomes".format(len(genome_marker_dict.keys())))
    # For each genome, write coordinates of each identified marker
    logging.info("Writing info on markers per genome")
    write_markers(genome_marker_dict, args.checkm_dir, args.out_dir, args.single_marker)

    # Read phylogeny for each bin
    phylo_file = find_file(args.checkm_dir, "phylogeny.tab")
    logging.info("Reading phylogeny info from {}".format(phylo_file))
    phylo_dict = get_phylogeny(phylo_file, args.genomes)

    # For each genome, figure out the lowest common rank that has a lineage-specific marker set in CheckM
    logging.info("Parsing linage specific sets for each genome")
    marker_sets = parse_checkm_taxon_list(args.checkm_taxon_list)
    genome_lineage_sets = get_genome_lineage_sets(phylo_dict, marker_sets)
    # Write a file with cluster_id in first column, comma-separated genome_ids in second column and the lineage specific
    # marker set in third column
    logging.info("Writing genome info to {}/genome_file.tab".format(args.out_dir))
    write_lineage_sets(genome_lineage_sets, args.out_dir)
    logging.info("Done.")


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("--checkm_dir", required=True,
                        help="CheckM output directory. Marker gene statistics and phylogeny files are assumed to be in \
                             'storage/marker_gene_stats.tsv' and 'phylogeny.tab', respectively.")
    parser.add_argument("--single_marker", action="store_true",
                        help="Only write coordinates for the longest identified marker region per ORF. \
                             Default: write all non-overlapping coordinates.")
    parser.add_argument("--checkm_taxon_list", required=True,
                        help="List of available taxonomic-specific marker sets. Generate this file by running \
                             checkm taxon_list")
    parser.add_argument("--out_dir", required=True,
                        help="Collate output in this directory. Each cluster will get a subdirectory under this path.")
    parser.add_argument("--genomes", nargs="+",
                        help="Specify genomes to limit parsing to.")
    args = parser.parse_args()
    main(args)
