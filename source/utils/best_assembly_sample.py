#!/usr/bin/env python

import pandas as pd
import os
from argparse import ArgumentParser
from glob import glob
import sys


def parse_samples(f):
    df = pd.read_table(f)
    samples = {}
    df.index = [df.iloc[i,0] + "_" + str(df.iloc[i,1]) for i in range(0,len(df))]
    for s in df.index:
        samples[s] = df.loc[s,"assemblyGroup"].split(",")
    return samples


def parse_assigned_reads(samples, result_dir):
    assigned = {}
    for sample, assemblies in samples.items():
        assigned[sample] = {}
        for a in assemblies:
            summary = glob(os.path.join(result_dir, "assembly", a, "mapping", "{}*.tab.summary".format(sample)))[0]
            with open(summary, 'r') as fhin:
                reads = int(fhin.readlines()[1].rstrip().split("\t")[1])
                assigned[sample][a] = reads
    return assigned


def get_best_assembly(samples, assigned_df):
    best_assembly = {}
    for sample in samples.keys():
        best = assigned_df.loc[:,sample].sort_values(ascending=False).head(1).index[0]
        best_assembly[sample] = best
    df = pd.DataFrame(best_assembly, index=["best_assembly"]).T
    df.index.name = "sample"
    return df


def unique_index(df):
    index = []
    df_cats = df.loc[:, df.dtypes == object].columns
    for j in df.index:
        index.append(";".join(df.loc[j, df_cats].values))
    df.index = index
    return df


def reset_index(df, cats):
    cols = {}
    for item in df.index:
        names = item.split(";")
        for i, name in enumerate(names):
            col_name = cats[i]
            try:
                cols[col_name].append(name)
            except KeyError:
                cols[col_name] = [name]
    df.index = list(range(0, len(df)))
    df = pd.merge(pd.DataFrame(cols)[cats], df, left_index=True, right_index=True)
    return df


def merge_files(best_assembly, results_dir, outdir):
    os.makedirs(outdir, exist_ok=True)
    files = []
    for t in ["tpm", "count"]:
        files += ["enzymes.parsed.{}.tab".format(t),
                  "kos.parsed.{}.tab".format(t),
                  "modules.parsed.{}.tab".format(t),
                  "pathways.parsed.{}.tab".format(t),
                  "modules.parsed.{}.normalized.tab".format(t),
                  "pathways.parsed.{}.normalized.tab".format(t),
                  "pfam.parsed.{}.tab".format(t)]
    for f in files:
        sys.stderr.write("Merging file {}...\n".format(f))
        df = pd.DataFrame()
        for assembly in best_assembly.best_assembly.unique():
            samples = best_assembly.loc[best_assembly.best_assembly==assembly].index
            sys.stderr.write("Getting results from {} samples in {}\n".format(len(samples), assembly))
            _df = pd.read_table(os.path.join(results_dir, "annotation", assembly, f))
            _df_samples = _df.loc[:,_df.dtypes!=object]
            _df_cats = _df.loc[:,_df.dtypes==object].columns
            # Extract wanted samples
            _df = pd.concat([_df.loc[:,_df_cats], _df_samples.loc[:,samples]], axis=1)
            # Set unique index for merging
            _df = unique_index(_df)
            _df.drop(_df_cats, axis=1, inplace=True)
            df = pd.merge(df, _df, left_index=True, right_index=True, how="outer")
        #Reset unique index
        df = reset_index(df, _df_cats)
        df.fillna(0, inplace=True)
        sys.stderr.write("Writing merged file {} to {}\n".format(f, outdir))
        df.to_csv(os.path.join(outdir,f), index=False, sep="\t")




def main():
    parser = ArgumentParser()
    parser.add_argument("sample_list", type=str, help="Sample list")
    parser.add_argument("results_dir", type=str, help="Results directory")
    parser.add_argument("--outdir", type=str, help="Output directory for merged results files")
    args = parser.parse_args()
    samples = parse_samples(args.sample_list)
    assigned = parse_assigned_reads(samples, args.results_dir)
    assigned_df = pd.DataFrame(assigned)
    assigned_df.fillna(0, inplace=True)
    best_assembly = get_best_assembly(samples, assigned_df)
    best_assembly.to_csv(sys.stdout, sep="\t")
    if args.outdir:
        merge_files(best_assembly, args.results_dir, args.outdir)


if __name__ == '__main__':
    main()
