#!/usr/bin/env python

from argparse import ArgumentParser
import gffutils

def reformat(gff, faa, gff_out, faa_out, genome_id):
    map = {}
    with open(gff, 'r') as fh, open(gff_out, 'w') as fh_out:
        for line in fh:
            if line[0] == "#":
                continue
            line = line.rstrip()
            feat = gffutils.feature.feature_from_line(line, strict=False, dialect=gffutils.constants.dialect,keep_order=False)
            # Reformat contig id
            contig_id = "{}|{}".format(genome_id,feat.seqid)
            attributes = []
            for a in feat.attributes:
                # Keep this original identifier
                if a == "ID":
                    attributes.append(a+" "+",".join(feat.attributes[a]))
                    contig_gene_num = feat.attributes[a][0].split("_")[-1]
                    attributes.append("gene_id {}_{}".format(feat.seqid,contig_gene_num))
                elif a == "locus_tag":
                    # Reformat the locus_tag identifier
                    locus_tag = feat.attributes[a][0]
                    reformatted_tag = "{}_{}".format(feat.seqid,locus_tag.replace(feat.seqid,""))
                    map[locus_tag] = reformatted_tag
                    attributes.append("{} {}".format("gene_id",reformatted_tag))
            attributes = "; ".join(attributes)
            fh_out.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(feat.seqid,feat.source,feat.featuretype,feat.start,feat.end,feat.score,feat.strand,feat.frame,attributes))

    if not faa:
        return

    with open(faa, 'r') as fh, open(faa_out, 'w') as fh_out:
        for line in fh:
            line = line.rstrip()
            if line[0] == ">":
                locus_tag = line.split(" ")[0].replace(">","")
                reformatted_tag = map[locus_tag]
                line = line.replace(">{}".format(locus_tag),">{}".format(reformatted_tag))
            fh_out.write("{}\n".format(line))


def main():

    parser = ArgumentParser()
    parser.add_argument("--gff", "GFF input")
    parser.add_argument("--faa", "Protein fasta input")
    parser.add_argument("--gff_out", "GFF output")
    parser.add_argument("-faa_out", "Protein fasta output")
    parser.add_argument("--genome_id", "Genome ID", required=True)

    args = parser.parse_args()

    reformat(args.gff, args.faa, args.gff_out, args.faa_out, args.genome_id)

if __name__ == "__main__":
    main()