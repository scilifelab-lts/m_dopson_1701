# Taxonomic classification of reads

## Database
All complete reference genomes for archaea, bacteria, fungi, invertebrate, plant, protozoa and viral were downloaded from the refseq database on April 3, 2018.
In total the database comprised 28,315 sequences spread over the superkingdom level as such:

| Name | Sequences | TotalSize (bp) |
| ---- | --------- | --------- |
| Archaea | 379 | 634,367,060 |
| Bacteria |  18,161 | 36,821,952,064 |
| Eukaryota | 190 | 365,525,180 |
| Viroids | 47 | 15,805 |
| Viruses | 9,538 |  254,547,986 |

## Classification

centrifuge was run as part of the NBIS-metagenomics workflow on preprocessed read-pairs from all 44 metagenomes using:

```
snakemake --cluster sbatch -A {cluster.account} -t {cluster.time} -p {cluster.partition} -n {cluster.n} -e {cluster.log} -o {cluster.log} {cluster.extra} --cluster-config mycluster.yaml -j 100 -k -r --configfile results/configs_used/config_binning_2000bp.yaml -p centrifuge_classify
```

Output was converted to kraken-style reports using `centrifuge-kreport` (*.kraken_report) and to krona plots (*.html).
